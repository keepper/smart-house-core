<?php
namespace Keepper\SmartHouse\Core\Tests\Sensor\System;

use Keepper\SmartHouse\Core\Sensor\System\DiskSpaceSensor;
use PHPUnit\Framework\TestCase;

class DiskSpaceSensorTest extends TestCase {

    public function testRead() {
        $sensor = new DiskSpaceSensor('test-sensor', '/');
        $value = $sensor->getValue();
        $this->assertTrue($value > 5);
    }

}