<?php
namespace Keepper\SmartHouse\Core\Tests\Sensor;

use Keepper\SmartHouse\Core\Sensor\AbstractSensor;
use Keepper\SmartHouse\Core\Sensor\Event\SensorEventSubscriberInterface;
use PHPUnit\Framework\TestCase;

class TimeSensor  extends AbstractSensor {

    protected function readValue() {
        $date = new \DateTime('now');
        return $date->format('H:i:s');
    }
}

class SensorTest extends TestCase {
    public function testGetValue() {
        $sensor = new TimeSensor('unit-test');
        $eventCounter = 0;
        $expected = 2;
        $sensor->subscriber()->addListener(SensorEventSubscriberInterface::CHANGED, function(string $uuid, $value) use (&$eventCounter){
            $eventCounter++;
            $this->assertEquals('unit-test', $uuid);
        });

        $initial = $sensor->getValue();
        $this->assertEquals(1, $eventCounter);

        $value = $sensor->getValue();
        if ($value == $initial) {
            $this->assertEquals(1, $eventCounter);
        } else {
            $this->assertEquals(2, $eventCounter);
            $expected = 3;
        }
        sleep(1);
        $value = $sensor->getValue();
        $this->assertNotEquals($initial, $value);
        $this->assertEquals($expected, $eventCounter);
    }
}