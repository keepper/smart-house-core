<?php
namespace Keepper\SmartHouse\Core\Tests\Registry;

use Keepper\SmartHouse\Core\Action\ActionInterface;
use Keepper\SmartHouse\Core\Button\Button;
use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Output\OutputSensorInterface;
use Keepper\SmartHouse\Core\Registry\Registry;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Keepper\SmartHouse\Core\Sensor\Event\SensorEventSubscriberInterface;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Toggle\SwitchInterface;
use Keepper\SmartHouse\Core\Uuid;
use PHPUnit\Framework\TestCase;

class RegistryTest extends TestCase {

    public function testAttachUuid() {
        $uuid = new Uuid('test-uuid');

        $registry = new Registry();
        $this->assertFalse($registry->hasUuid($uuid->uuid()));
        $registry->attachUuid($uuid);
        $this->assertTrue($registry->hasUuid($uuid->uuid()));
    }

    /**
     * @expectedException \Exception
     */
    public function testErrorOnAttachExistsUuid() {
        $uuid = new Uuid('test-uuid');

        $registry = new Registry();
        $registry->attachUuid($uuid);
        $this->assertTrue(true);
        $registry->attachUuid(new Button('test-uuid'));
    }

    public function testGetByUuid() {
        $uuid = new Uuid('test-uuid');

        $registry = new Registry();
        $this->assertNull($registry->getByUuid($uuid->uuid()));
        $registry->attachUuid($uuid);
        $this->assertEquals($uuid, $registry->getByUuid($uuid->uuid()));
    }

    /**
     * @dataProvider dataProviderForErrorOnGetButton
     * @expectedException \Exception
     */
    public function testErrorOnGetButton($uuid) {
        $registry = new Registry();
        $registry->attachUuid(new Uuid('test-uuid-2'));

        $registry->getButton($uuid);
    }

    public function dataProviderForErrorOnGetButton() {
        return [
            ['test-uuid-1'],
            ['test-uuid-2']
        ];
    }

    public function testGetButtons() {
        $registry = new Registry();
        $registry->attachUuid(new Uuid('some-uuid'));
        $this->assertCount(0, $registry->getButtons());

        $button = new Button('test-button');
        $registry->attachUuid($button);
        $this->assertCount(1, $registry->getButtons());
        $this->assertEquals('test-button', $registry->getButtons()[0]->uuid());

        $registry->attachUuid(new Button('another-button'));
        $this->assertCount(2, $registry->getButtons());
        $this->assertEquals('another-button', $registry->getButtons()[1]->uuid());
    }

    /**
     * @dataProvider dataProviderForGetButton
     */
    public function testGetButton($uuid) {
        $registry = new Registry();
        $registry->attachUuid(new Button('test-button-1'));
        $registry->attachUuid(new Button('test-button-2'));

        $this->assertEquals($uuid, $registry->getButton($uuid)->uuid());
    }

    public function dataProviderForGetButton() {
        return [
            ['test-button-1'],
            ['test-button-2']
        ];
    }

    /**
     * @dataProvider dataProviderForGetConcretes
     */
    public function testGetConcretes($method, $class) {
        if ($method == 'getDigitalSensor') {
            $this->markTestSkipped();
            return;
        }
        $method .= 's';

        $registry = new Registry();
        $registry->attachUuid(new Uuid('some-uuid'));
        $this->assertCount(0, $registry->$method());

        $sensor = $this->getMockBuilder($class)->getMock();
        $sensor->method('uuid')->willReturn('test-uuid');

        $registry->attachUuid($sensor);
        $this->assertCount(1, $registry->$method());
        $this->assertEquals($sensor, $registry->$method()[0]);
    }

    /**
     * @dataProvider dataProviderForGetConcrete
     */
    public function testGetConcrete($method, $class) {
        $sensor = $this->getMockBuilder($class)->getMock();
        $sensor->method('uuid')->willReturn('test-uuid');
        $registry = new Registry();
        $registry->attachUuid($sensor);
        $this->assertEquals($sensor, $registry->$method('test-uuid'));
    }

    public function dataProviderForGetConcrete() {
        $result = $this->dataProviderForGetConcretes();
        $result[] = ['getDigitalSensor', DigitalSensorInterface::class];
        return $result;
    }

    public function dataProviderForGetConcretes() {
        return [
            ['getButton', ButtonInterface::class],
            ['getSensor', SensorInterface::class],
            ['getSwitch', SwitchInterface::class],
            ['getOutput', OutputSensorInterface::class]
        ];
    }

    public function testAtachAction(){
        $action = $this->getMockBuilder(ActionInterface::class)->getMock();
        $registry = new Registry();
        $registry->attachAction($action);
        $this->assertTrue(true);
    }
}