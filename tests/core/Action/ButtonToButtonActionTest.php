<?php
namespace Keepper\SmartHouse\Core\Tests\Action;

use Keepper\SmartHouse\Core\Action\ButtonToButtonAction;
use Keepper\SmartHouse\Core\Button\Button;
use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriber;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use PHPUnit\Framework\TestCase;

class ButtonToButtonActionTest extends TestCase {

    /**
     * @dataProvider dataProviderForAction
     */
    public function testAction($mode) {
        $mainButton = new Button('first-button');
        $relayButton = new Button('second-button');

        $this->assertFalse( $mainButton->subscriber()->hasListeners(ButtonEventSubscriber::PRESSED));

        $action = new ButtonToButtonAction($mainButton, $relayButton);

        $this->assertTrue( $mainButton->subscriber()->hasListeners(ButtonEventSubscriber::PRESSED));

        $isFired = false;
        $relayButton->subscriber()->addListener(ButtonEventSubscriber::PRESSED, function($uuid, $mode) use (&$isFired){
            $isFired = true;
            $this->assertEquals('second-button', $uuid);
        });

        $mainButton->press($mode);
        $this->assertTrue($isFired, 'Ожидали, что будет нажата связанная кнопка');
    }

    public function dataProviderForAction() {
        return [
            [PressModeInterface::SINGLE],
            [PressModeInterface::DOUBLE],
            [PressModeInterface::LONG]
        ];
    }

    public function testSetName() {
        $mainButton = new Button('first-button');
        $relayButton = new Button('second-button');

        $action = new ButtonToButtonAction($mainButton, $relayButton);

        $this->assertEquals('ButtonToButton', $action->name());
        $action->setName('Some name');
        $this->assertEquals('Some name', $action->name());
    }
}