<?php
namespace Keepper\SmartHouse\Core\Tests\Action;

use Keepper\SmartHouse\Core\Action\ButtonToButtonAction;
use Keepper\SmartHouse\Core\Action\ButtonToSwitchAction;
use Keepper\SmartHouse\Core\Button\Button;
use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriber;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriberInterface;
use Keepper\SmartHouse\Core\Toggle\ToggleSwitch;
use PHPUnit\Framework\TestCase;

class ButtonToSwitchActionTest extends TestCase {

    /**
     * @dataProvider dataProviderForAction
     */
    public function testAction($mode, $expected) {
        $mainButton = new Button('first-button');
        $switch = new ToggleSwitch('switch');

        $this->assertFalse( $mainButton->subscriber()->hasListeners(ButtonEventSubscriber::PRESSED));

        $action = new ButtonToSwitchAction($mainButton, $switch);

        $this->assertTrue( $mainButton->subscriber()->hasListeners(ButtonEventSubscriber::PRESSED));

        $isFired = false;
        $switch->subscriber()->addListener(SwitchEventSubscriberInterface::CHANGED, function($uuid, $v) use (&$isFired){
            $isFired = true;
            $this->assertEquals('switch', $uuid);
        });

        $mainButton->press($mode);
        $this->assertEquals($expected, $isFired);
    }

    public function dataProviderForAction() {
        return [
            [PressModeInterface::SINGLE, true],
            [PressModeInterface::DOUBLE, false],
            [PressModeInterface::LONG, false]
        ];
    }

    public function testSetName() {
        $mainButton = new Button('first-button');
        $switch = new ToggleSwitch('switch');
        $action = new ButtonToSwitchAction($mainButton, $switch);

        $this->assertEquals('ButtonToSwitch', $action->name());
        $action->setName('Some name');
        $this->assertEquals('Some name', $action->name());
    }
}