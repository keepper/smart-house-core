<?php
namespace Keepper\SmartHouse\Core\Tests\Toggle;

use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriberInterface;
use Keepper\SmartHouse\Core\Toggle\ToggleSwitch;
use PHPUnit\Framework\TestCase;

class ToggleSwithTest extends TestCase {

    /**
     * @dataProvider dataProviderForDefaultState
     */
    public function testDefaultState($expected) {
        $switch = new ToggleSwitch('some-uuid', $expected);
        $this->assertEquals($expected, $switch->state());
    }

    public function dataProviderForDefaultState() {
        return [
            [true],
            [false]
        ];
    }

    /**
     * @dataProvider dataProviderForTurn
     */
    public function testTurn($method, $defaultState, $expectedState, $expectedChangeEvent, $expectedOnEvent, $expectedOffEvent) {
        $expecteduuid = 'some-uuid-'.rand(1,10);
        $switch = new ToggleSwitch($expecteduuid, $defaultState);
        $changeFired = false;
        $offFired = false;
        $onFired = false;

        $switch->subscriber()->addListener(SwitchEventSubscriberInterface::CHANGED, function(string $uuid, bool $state) use (&$changeFired, $expecteduuid, $expectedState){
            $changeFired = true;
            $this->assertEquals($expecteduuid, $uuid);
            $this->assertEquals($expectedState, $state);
        });

        $switch->subscriber()->addListener(SwitchEventSubscriberInterface::TURNED_ON, function(string $uuid) use (&$onFired, $expecteduuid){
            $onFired = true;
            $this->assertEquals($expecteduuid, $uuid);
        });

        $switch->subscriber()->addListener(SwitchEventSubscriberInterface::TURNED_OFF, function(string $uuid) use (&$offFired, $expecteduuid){
            $offFired = true;
            $this->assertEquals($expecteduuid, $uuid);
        });

        $switch->$method();

        $this->assertEquals($expectedChangeEvent, $changeFired);
        $this->assertEquals($expectedOnEvent, $onFired);
        $this->assertEquals($expectedOffEvent, $offFired);
        $this->assertEquals($expectedState, $switch->state());
    }

    public function dataProviderForTurn() {
        return [
            ['turnOn', false, true, true, true, false],
            ['turnOff', true, false, true, false, true],
            ['toggle', false, true, true, true, false],
            ['toggle', true, false, true, false, true],
        ];
    }
}