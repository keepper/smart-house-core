<?php
namespace Keepper\SmartHouse\Core\Tests\Button;

use Keepper\SmartHouse\Core\Button\Button;
use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriberInterface;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use PHPUnit\Framework\TestCase;

class ButtonTest extends TestCase {

    /**
     * @dataProvider dataProviderForGetUuid
     */
    public function testGetUuid($expected) {
        $button = new Button($expected);

        $this->assertEquals($expected, $button->uuid());
    }

    public function dataProviderForGetUuid() {
        return [
            ['test-uuid-'.rand(1,100)],
            ['test-uuid-'.rand(100,1000)],
        ];
    }

    /**
     * @dataProvider dataProviderForPressEvent
     */
    public function testPressEvent($expectedMode) {
        $button = new Button('unit-test-button');
        $hasFired = false;
        $button->subscriber()->addListener(ButtonEventSubscriberInterface::PRESSED, function(string $uuid, int $mode) use ($expectedMode, &$hasFired) {
            $hasFired = true;
            $this->assertEquals($expectedMode, $mode);
            $this->assertEquals('unit-test-button', $uuid);
        });

        $button->press($expectedMode);
        $this->assertTrue($hasFired);
    }

    public function dataProviderForPressEvent() {
        return [
            [PressModeInterface::SINGLE],
            [PressModeInterface::DOUBLE],
            [PressModeInterface::LONG],
        ];
    }
}