<?php
namespace Keepper\SmartHouse\Core\Registry;

use Keepper\SmartHouse\Core\Action\ActionInterface;
use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Output\OutputSensorInterface;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Toggle\SwitchInterface;
use Keepper\SmartHouse\Core\UuidInterface;

class Registry implements RegistryInterface {
    private $devices = [];
    private $actions = [];

    public function attachUuid(UuidInterface $device) {
        if (array_key_exists($device->uuid(), $this->devices) && $this->devices[$device->uuid()] != $device) {
            throw new \Exception('Указанный uuid ('.$device->uuid().') уже зарегистрирован');
        }

        $this->devices[$device->uuid()] = $device;
    }

    public function getByUuid(string $uuid): ?UuidInterface {
        if ( !$this->hasUuid($uuid) ) {
            return null;
        }

        return $this->devices[$uuid];
    }

    /**
     * @return ButtonInterface[]
     */
    public function getButtons(): array {
        return $this->getByClassName(ButtonInterface::class);
    }

    protected function getByClassName($className) {
        $result = [];

        foreach ($this->devices as $uuid => $device) {
            if ( !($device instanceof $className) ) {
                continue;
            }

            $result[] = $device;
        }

        return $result;
    }

    public function getButton(string $uuid): ButtonInterface {
        return $this->getInstanceByUuid($uuid, ButtonInterface::class);
    }

    protected function getInstanceByUuid(string $uuid, $className) {
        if ( !$this->hasUuid($uuid) ) {
            throw new \Exception('Указанный uuid ('.$uuid.') не зарегистрирован');
        }

        $device = $this->devices[$uuid];
        if ( !($device instanceof $className) ) {
            throw new \Exception('Устройство с указанным uuid ('.$device->uuid().') не является '.$className);
        }

        return $device;
    }

    public function getDigitalSensor(string $uuid): DigitalSensorInterface {
        return $this->getInstanceByUuid($uuid, DigitalSensorInterface::class);
    }

    public function getSensor(string $uuid): SensorInterface {
        return $this->getInstanceByUuid($uuid, SensorInterface::class);
    }

    /**
     * @return SensorInterface[]
     */
    public function getSensors(): array {
        return $this->getByClassName(SensorInterface::class);
    }

    public function getSwitch(string $uuid): SwitchInterface {
        return $this->getInstanceByUuid($uuid, SwitchInterface::class);
    }

    /**
     * @return SwitchInterface[]
     */
    public function getSwitchs(): array {
        return $this->getByClassName(SwitchInterface::class);
    }

    public function attachAction(ActionInterface $action) {
        $this->actions[] = $action;
    }

    public function getOutput(string $uuid): OutputSensorInterface {
        return $this->getInstanceByUuid($uuid, OutputSensorInterface::class);
    }

    /**
     * @return OutputSensorInterface[]
     */
    public function getOutputs(): array {
        return $this->getByClassName(OutputSensorInterface::class);
    }

    public function hasUuid(string $uuid): bool {
        return array_key_exists($uuid, $this->devices);
    }
}