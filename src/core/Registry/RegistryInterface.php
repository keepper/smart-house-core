<?php
namespace Keepper\SmartHouse\Core\Registry;

use Keepper\SmartHouse\Core\Action\ActionInterface;
use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Output\OutputSensorInterface;
use Keepper\SmartHouse\Core\Sensor\DigitalSensorInterface;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Toggle\SwitchInterface;
use Keepper\SmartHouse\Core\UuidInterface;

interface RegistryInterface {

    public function attachUuid(UuidInterface $device);

    public function attachAction(ActionInterface $action);

    public function getByUuid(string $uuid): ?UuidInterface;

    public function getButton(string $uuid): ButtonInterface;

    public function getDigitalSensor(string $uuid): DigitalSensorInterface;

    public function getSensor(string $uuid): SensorInterface;

    public function getSwitch(string $uuid): SwitchInterface;

    public function getOutput(string $uuid): OutputSensorInterface;

    public function hasUuid(string $uuid): bool;

    /**
     * @return SensorInterface[]
     */
    public function getSensors(): array;

    /**
     * @return ButtonInterface[]
     */
    public function getButtons(): array;

    /**
     * @return SwitchInterface[]
     */
    public function getSwitchs(): array;

    /**
     * @return OutputSensorInterface[]
     */
    public function getOutputs(): array;
}