<?php
namespace Keepper\SmartHouse\Core\Toggle;

use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriberInterface;
use Keepper\SmartHouse\Core\UuidInterface;

interface SwitchInterface extends UuidInterface {

    /**
     * Возвращает текущее значение
     * @return bool
     */
    public function state(): bool;

    /**
     * Включает преключатель
     * @return mixed
     */
    public function turnOn();

    /**
     * Выключает переключатель
     * @return mixed
     */
    public function turnOff();

    /**
     * Изменяет текущее состояние переключателя на противоположное
     * @return mixed
     */
    public function toggle();

    /**
     * Возвращает объект для подписки на события
     * @return SwitchEventSubscriberInterface
     */
    public function subscriber(): SwitchEventSubscriberInterface;
}