<?php
namespace Keepper\SmartHouse\Core\Toggle;

use Keepper\Lib\Events\EventDispatcher;
use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\SmartHouse\Core\Storage\NullStateStorage;
use Keepper\SmartHouse\Core\Storage\StateStoragedInterface;
use Keepper\SmartHouse\Core\Storage\StateStoragedTrait;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriber;
use Keepper\SmartHouse\Core\Toggle\Event\SwitchEventSubscriberInterface;
use Keepper\SmartHouse\Core\Uuid;

class ToggleSwitch extends Uuid implements SwitchInterface, StateStoragedInterface {

    use StateStoragedTrait;

    protected $dispatcher;

    protected $state = false;

    public function __construct(
        string $uuid,
        bool $defaultState = false,
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        parent::__construct($uuid);
        $this->setStorage($stateStorage ?? new NullStateStorage());
        $this->state = $this->stateStorage->getLastValue($uuid) ?? $defaultState;
        $this->dispatcher = $dispatcher ?? new EventDispatcher(new SwitchEventSubscriber());
    }

    /**
     * @inheritdoc
     */
    public function state(): bool {
        return $this->state;
    }

    /**
     * @inheritdoc
     */
    public function turnOn() {
        if ($this->state()) {
            return;
        }

        $this->state = true;
        $this->stateStorage->saveValue($this->uuid(), $this->state);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_ON, [$this->uuid()]);
    }

    /**
     * @inheritdoc
     */
    public function turnOff() {
        if (!$this->state()) {
            return;
        }

        $this->state = false;
        $this->stateStorage->saveValue($this->uuid(), $this->state);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);
        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_OFF, [$this->uuid()]);
    }

    /**
     * @inheritdoc
     */
    public function toggle() {
        $this->state = !$this->state;
        $this->stateStorage->saveValue($this->uuid(), $this->state);

        $this->dispatcher->dispatch(SwitchEventSubscriberInterface::CHANGED, [$this->uuid(), $this->state]);

        if ($this->state) {
            $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_ON, [$this->uuid()]);
        } else {
            $this->dispatcher->dispatch(SwitchEventSubscriberInterface::TURNED_OFF, [$this->uuid()]);
        }
    }

    /**
     * @inheritdoc
     */
    public function subscriber(): SwitchEventSubscriberInterface {
        return $this->dispatcher->subscriber();
    }
}