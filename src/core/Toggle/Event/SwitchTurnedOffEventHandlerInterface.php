<?php
namespace Keepper\SmartHouse\Core\Toggle\Event;


interface SwitchTurnedOffEventHandlerInterface {
    public function onSwitchTurnedOff(string $uuid);
}