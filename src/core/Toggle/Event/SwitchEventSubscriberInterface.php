<?php
namespace Keepper\SmartHouse\Core\Toggle\Event;

use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

interface SwitchEventSubscriberInterface extends EventSubscriberInterface {
    const CHANGED = 'switch-changed';
    const TURNED_ON = 'switch-turned-on';
    const TURNED_OFF = 'switch-turned-off';

    public function bindChanged(SwitchChangedEventHandlerInterface $handler);
    public function bindTurnedOn(SwitchTurnedOnEventHandlerInterface $handler);
    public function bindTurnedOff(SwitchTurnedOffEventHandlerInterface $handler);
}