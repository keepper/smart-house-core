<?php
namespace Keepper\SmartHouse\Core\Toggle\Event;

interface SwitchTurnedOnEventHandlerInterface {
    public function onSwitchTurnedOn(string $uuid);
}