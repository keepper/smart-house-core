<?php
namespace Keepper\SmartHouse\Core\Toggle\Event;

interface SwitchChangedEventHandlerInterface {
    public function onSwitchChanged(string $uuid, bool $state);
}