<?php
namespace Keepper\SmartHouse\Core\Toggle\Event;

use Keepper\Lib\Events\EventSubscriber;

class SwitchEventSubscriber extends EventSubscriber implements SwitchEventSubscriberInterface {

    public function bindChanged(SwitchChangedEventHandlerInterface $handler) {
        $this->addListener(self::CHANGED, [$handler, 'onSwitchChanged']);
    }

    public function bindTurnedOn(SwitchTurnedOnEventHandlerInterface $handler) {
        $this->addListener(self::CHANGED, [$handler, 'onSwitchTurnedOn']);
    }

    public function bindTurnedOff(SwitchTurnedOffEventHandlerInterface $handler) {
        $this->addListener(self::CHANGED, [$handler, 'onSwitchTurnedOff']);
    }
}