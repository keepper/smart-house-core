<?php
namespace Keepper\SmartHouse\Core\Output;

use Keepper\SmartHouse\Core\UuidInterface;

interface OutputSensorInterface extends UuidInterface {
    public function write($value);
}