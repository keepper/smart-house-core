<?php
namespace Keepper\SmartHouse\Core;

interface UuidInterface {

    /**
     * Уникальный идентификатор
     * @return string
     */
    public function uuid(): string;
}