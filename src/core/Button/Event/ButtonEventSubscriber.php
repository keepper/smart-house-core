<?php
namespace Keepper\SmartHouse\Core\Button\Event;


use Keepper\Lib\Events\EventSubscriber;

class ButtonEventSubscriber extends EventSubscriber implements ButtonEventSubscriberInterface {

    public function bindPressed(ButtonPressedEventHandlerInterface $handler) {
        $this->addListener(self::PRESSED, [$handler, 'onPressed']);
    }
}