<?php
namespace Keepper\SmartHouse\Core\Button\Event;

interface ButtonPressedEventHandlerInterface {

    /**
     * Обработчик события нажатия кнопки
     * @param string $uuid
     * @param int $mode
     * @return mixed
     */
    public function onPressed(string $uuid, int $mode);
}