<?php
namespace Keepper\SmartHouse\Core\Button\Event;

use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

interface ButtonEventSubscriberInterface extends EventSubscriberInterface {
    const PRESSED = 'pressed';

    public function bindPressed(ButtonPressedEventHandlerInterface $handler);
}