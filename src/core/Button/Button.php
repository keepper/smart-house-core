<?php
namespace Keepper\SmartHouse\Core\Button;

use Keepper\Lib\Events\EventDispatcher;
use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriber;
use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriberInterface;
use Keepper\SmartHouse\Core\Uuid;

class Button extends Uuid implements ButtonInterface {

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    public function __construct(
        string $uuid,
        EventDispatcherInterface $dispatcher = null
    ) {
        parent::__construct($uuid);
        $this->dispatcher = $dispatcher ?? new EventDispatcher(new ButtonEventSubscriber());
    }

    /**
     * @inheritdoc
     */
    public function press(int $mode = PressModeInterface::SINGLE) {
        $this->dispatcher->dispatch(ButtonEventSubscriberInterface::PRESSED, [$this->uuid(), $mode]);
    }

    /**
     * @inheritdoc
     */
    public function subscriber(): ButtonEventSubscriberInterface {
        return $this->dispatcher->subscriber();
    }
}