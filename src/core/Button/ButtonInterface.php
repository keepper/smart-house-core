<?php
namespace Keepper\SmartHouse\Core\Button;

use Keepper\SmartHouse\Core\Button\Event\ButtonEventSubscriberInterface;
use Keepper\SmartHouse\Core\UuidInterface;

interface ButtonInterface extends UuidInterface {

    /**
     * Нажатие кнопки
     * @param int $mode
     * @return mixed
     */
    public function press(int $mode = PressModeInterface::SINGLE);

    /**
     * Возвращает объект для подписки на события
     * @return ButtonEventSubscriberInterface
     */
    public function subscriber(): ButtonEventSubscriberInterface;
}