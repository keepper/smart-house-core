<?php
namespace Keepper\SmartHouse\Core\Button;

interface PressModeInterface {

    /**
     * Обычное одиночное нажатие
     */
    const SINGLE = 1;

    /**
     * Двойное нажатие
     */
    const DOUBLE = 2;

    /**
     * Длинное нажатие
     */
    const LONG = 3;
}