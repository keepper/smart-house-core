<?php
namespace Keepper\SmartHouse\Core\Action;

interface ActionInterface {

    /**
     * Возвращает название
     * @return string
     */
    public function name(): string;
}