<?php
namespace Keepper\SmartHouse\Core\Action;

use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Button\Event\ButtonPressedEventHandlerInterface;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use Keepper\SmartHouse\Core\Toggle\SwitchInterface;

class ButtonToSwitchAction implements ActionInterface, ButtonPressedEventHandlerInterface {

    /**
     * @var SwitchInterface
     */
    private $switch;

    private $name = 'ButtonToSwitch';

    public function __construct(
        ButtonInterface $lightButton,
        SwitchInterface $lightSwitch
    ) {
        $lightButton->subscriber()->bindPressed($this);
        $this->switch = $lightSwitch;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @inheritdoc
     */
    public function name(): string {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function onPressed(string $uuid, int $mode) {
        if ( $mode != PressModeInterface::SINGLE ) {
            return;
        }
        $this->switch->toggle();
    }

}