<?php
namespace Keepper\SmartHouse\Core\Action;

use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Button\Event\ButtonPressedEventHandlerInterface;

class ButtonToButtonAction implements ActionInterface, ButtonPressedEventHandlerInterface {

    /**
     * @var ButtonInterface
     */
    private $button;

    private $name = 'ButtonToButton';

    public function __construct(
        ButtonInterface $mainButton,
        ButtonInterface $secondButton
    ) {
        $mainButton->subscriber()->bindPressed($this);
        $this->button = $secondButton;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @inheritdoc
     */
    public function name(): string {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function onPressed(string $uuid, int $mode) {
        $this->button->press($mode);
    }
}