<?php
namespace Keepper\SmartHouse\Core\Storage;

trait StateStoragedTrait {
    /**
     * @var StateStorageInterface|null
     */
    protected $stateStorage;

    public function setStorage(StateStorageInterface $storage) {
        $this->stateStorage = $storage;
    }
}