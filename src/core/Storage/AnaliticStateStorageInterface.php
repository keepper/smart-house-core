<?php
namespace Keepper\SmartHouse\Core\Storage;

interface AnaliticStateStorageInterface extends StateStorageInterface {

    /**
     * Возвращает аналитический отчет по цифровым (значение сенсора цифра) сенсорам
     * @param string $uuid
     * @param \DateTimeInterface|null $from Начало временного интервала
     * @param \DateTimeInterface|null $to   Конец временного интервала
     * @return AverageRecordInterface|null
     */
    public function getByUuid(
        string $uuid,
        \DateTimeInterface $from = null,
        \DateTimeInterface $to = null): ?AverageRecordInterface;
}