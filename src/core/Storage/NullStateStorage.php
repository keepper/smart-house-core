<?php
namespace Keepper\SmartHouse\Core\Storage;

class NullStateStorage implements StateStorageInterface {

    /**
     * @inheritdoc
     */
    public function getLastValue(string $uuid) {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function saveValue(string $uuid, $value) {

    }
}