<?php
namespace Keepper\SmartHouse\Core\Storage;

interface AverageRecordInterface {

    /**
     * Последнее значение
     * @return mixed
     */
    public function lastValue();

    /**
     * Начало временного интервала выборки данных
     * @return \DateTimeInterface
     */
    public function startedAt(): \DateTimeInterface;

    /**
     * Конец временного интервала выборки данных
     * @return \DateTimeInterface
     */
    public function endedAt(): \DateTimeInterface;

    /**
     * Количество точек измерения
     * @return int
     */
    public function count(): int;

    /**
     * Среднее значение за указанный интервал
     * @return float
     */
    public function avg();

    /**
     * Минимальное значение за указанный интервал
     * @return mixed
     */
    public function minValue();

    /**
     * Максимальное значение за указанный интервал
     * @return mixed
     */
    public function maxValue();
}