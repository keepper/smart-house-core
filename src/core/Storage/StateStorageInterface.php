<?php
namespace Keepper\SmartHouse\Core\Storage;

interface StateStorageInterface {

    /**
     * Возвращает последнее сохраненное значение по uuid сенсора или устройства.
     * Если сохраненного значения нет, возвращает null
     * @param string $uuid
     * @return mixed|null
     */
    public function getLastValue(string $uuid);

    /**
     * Сохраняет значение от устройства
     * @param string $uuid
     * @param $value
     * @return mixed
     */
    public function saveValue(string $uuid, $value);
}