<?php
namespace Keepper\SmartHouse\Core\Storage;


class ProcessMemoryStateStorage implements StateStorageInterface {

    protected $values = [];

    /**
     * @inheritdoc
     */
    public function getLastValue(string $uuid) {
        if ( array_key_exists($uuid, $this->values) ) {
            return $this->values[$uuid];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function saveValue(string $uuid, $value) {
        $this->values[$uuid] = $value;
    }
}