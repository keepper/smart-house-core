<?php
namespace Keepper\SmartHouse\Core\Sensor;

use Keepper\SmartHouse\Core\Sensor\Event\SensorEventSubscriberInterface;
use Keepper\SmartHouse\Core\UuidInterface;

interface SensorInterface extends UuidInterface {
    /**
     * Возвращает значение датчика
     * @return mixed
     */
    public function getValue();

    /**
     * Возвращает объект для подписки на события
     * @return SensorEventSubscriberInterface
     */
    public function subscriber(): SensorEventSubscriberInterface;
}