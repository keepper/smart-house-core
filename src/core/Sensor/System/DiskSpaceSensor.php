<?php
namespace Keepper\SmartHouse\Core\Sensor\System;

use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\SmartHouse\Core\Sensor\AbstractSensor;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;

class DiskSpaceSensor extends AbstractSensor implements SensorInterface {
    private $device;

    public function __construct(
        string $uuid,
        string $device = '/',
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->device = $device;
        parent::__construct($uuid, $stateStorage, $dispatcher);
    }

    protected function readValue() {
        exec('df -h', $result);
        foreach ($result as $rows) {
            $rows = preg_replace('/\s{2,}/', ' ', $rows);
            $rows = explode(' ', $rows);
            if (count($rows) != 6) {
                continue;
            }
            list($filesystem, $size, $used, $available, $useInPercent, $mounted) = $rows;

            if ($mounted != $this->device) {
                continue;
            }

            return (int) str_replace('%', '', $useInPercent);
        }

        return null;
    }
}