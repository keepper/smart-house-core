<?php
namespace Keepper\SmartHouse\Core\Sensor;

interface DigitalSensorInterface extends SensorInterface {

	/**
	 * Возвращает значение датчика
	 * @return bool
	 */
	public function getValue(): bool;
}