<?php
namespace Keepper\SmartHouse\Core\Sensor;

use Keepper\Lib\Events\EventDispatcher;
use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\SmartHouse\Core\Sensor\Event\SensorEventSubscriber;
use Keepper\SmartHouse\Core\Sensor\Event\SensorEventSubscriberInterface;
use Keepper\SmartHouse\Core\Storage\ProcessMemoryStateStorage;
use Keepper\SmartHouse\Core\Storage\StateStoragedInterface;
use Keepper\SmartHouse\Core\Storage\StateStoragedTrait;
use Keepper\SmartHouse\Core\Storage\StateStorageInterface;
use Keepper\SmartHouse\Core\Uuid;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

abstract class AbstractSensor extends Uuid implements SensorInterface, StateStoragedInterface {
    use LoggerAwareTrait;

    use StateStoragedTrait;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    public function __construct(
        string $uuid,
        StateStorageInterface $stateStorage = null,
        EventDispatcherInterface $dispatcher = null
    ) {
        parent::__construct($uuid);
        $this->setStorage($stateStorage ?? new ProcessMemoryStateStorage());
        $this->dispatcher = $dispatcher ?? new EventDispatcher(new SensorEventSubscriber());
        $this->setLogger(new NullLogger());
    }

    abstract protected function readValue();

    /**
     * @inheritdoc
     */
    public function getValue() {
        try {
            $value = $this->readValue();
        } catch (\Exception $e) {
            $this->logger->error('Ошибка чтения данных сенсора. '.$e->getMessage()."\n".$e->getTraceAsString());
            return null;
        }

        if ( is_null($value) ) {
            $this->logger->warning('Ошибка чтения данных сенсора. Прочитано NULL значение');
            return null;
        }

        $this->dispatcher->dispatch(SensorEventSubscriberInterface::READED, [$this->uuid(), $value]);
        if ($this->stateStorage->getLastValue($this->uuid()) != $value) {
            $this->dispatcher->dispatch(SensorEventSubscriberInterface::CHANGED, [$this->uuid(), $value]);
        }

        try {
            $this->stateStorage->saveValue($this->uuid(), $value);
        } catch (\Exception $e) {
            $this->logger->error('Ошибка записи значения в хранилище. '.$e->getMessage()."\n".$e->getTraceAsString());
        }

        return $value;
    }

    /**
     * @inheritdoc
     */
    public function subscriber(): SensorEventSubscriberInterface {
        return $this->dispatcher->subscriber();
    }
}