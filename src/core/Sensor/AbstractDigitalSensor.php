<?php
namespace Keepper\SmartHouse\Core\Sensor;


abstract class AbstractDigitalSensor extends AbstractSensor implements DigitalSensorInterface {
    public function getValue(): bool {
        return (bool) parent::getValue();
    }
}