<?php
namespace Keepper\SmartHouse\Core\Sensor\Event;

use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

interface SensorEventSubscriberInterface extends EventSubscriberInterface {
    const CHANGED = 'sensor-value-changed';
    const READED = 'sensor-value-read';

    public function bindSensorValueChanged(SensorValueChangedEventHandlerInterface $handler);

    public function bindSensorValueRead(SensorValueReadEventHandlerInterface $handler);
}