<?php
namespace Keepper\SmartHouse\Core\Sensor\Event;

interface SensorValueChangedEventHandlerInterface {

    public function onSensorValueChanged(string $uuid, $value);
}