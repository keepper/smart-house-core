<?php
namespace Keepper\SmartHouse\Core\Sensor\Event;

use Keepper\Lib\Events\EventSubscriber;

class SensorEventSubscriber extends EventSubscriber implements SensorEventSubscriberInterface {

    public function bindSensorValueChanged(SensorValueChangedEventHandlerInterface $handler) {
        $this->addListener(self::CHANGED, [$handler, 'onSensorValueChanged']);
    }

    public function bindSensorValueRead(SensorValueReadEventHandlerInterface $handler) {
        $this->addListener(self::READED, [$handler, 'onSensorValueRead']);
    }
}