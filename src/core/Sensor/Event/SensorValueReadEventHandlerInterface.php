<?php
namespace Keepper\SmartHouse\Core\Sensor\Event;

interface SensorValueReadEventHandlerInterface {

    public function onSensorValueRead(string $uuid, $value);
}