<?php
namespace Keepper\SmartHouse\Core;

class Uuid implements UuidInterface {
    protected $uuid;

    public function __construct(
        string $uuid
    ) {
        $this->uuid = $uuid;
    }

    /**
     * @inheritdoc
     */
    public function uuid(): string {
        return $this->uuid;
    }
}