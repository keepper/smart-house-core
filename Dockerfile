# Для начала указываем исходный образ, он будет использован как основа
FROM php:7.2-fpm-alpine3.8

ENV PHPIZE_DEPS \
    autoconf \
    cmake \
    file \
    g++ \
    gcc \
    libc-dev \
    pcre-dev \
    make \
    git \
    pkgconf \
    re2c \
    wget \
    bash


RUN set -xe \
    && apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure mysqli --with-mysqli \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-configure mbstring --enable-mbstring \
    && docker-php-ext-install -j$(nproc) \
        bcmath \
        pcntl \
        mysqli \
        pdo_mysql \
        mbstring \
        iconv

RUN wget https://getcomposer.org/installer -O /tmp/composer-setup.php \
    && wget https://composer.github.io/installer.sig -O /tmp/composer-setup.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer --snapshot \
    && rm -rf /tmp/composer-setup.php /tmp/composer-setup.sig

# Copy configuration
#COPY config/php7.ini /usr/local/etc/php/conf.d/
RUN apk del .build-deps \
    && rm -rf /tmp/* \
    && rm -rf /app \
    && mkdir /app

RUN adduser -D -g '' developer

# Possible values for ext-name:
# bcmath bz2 calendar ctype curl dba dom enchant exif fileinfo filter ftp gd gettext gmp hash iconv imap interbase intl
# json ldap mbstring mcrypt mssql mysql mysqli oci8 odbc opcache pcntl pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci
# pdo_odbc pdo_pgsql pdo_sqlite pgsql phar posix pspell readline recode reflection session shmop simplexml snmp soap
# sockets spl standard sybase_ct sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader xmlrpc xmlwriter xsl zip

#COPY config/php7.ini /usr/local/etc/php/conf.d/
#COPY config/fpm/php-fpm.conf /usr/local/etc/
#COPY config/fpm/pool.d /usr/local/etc/pool.d

USER developer
VOLUME ["/var/www"]
WORKDIR /var/www